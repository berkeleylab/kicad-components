(kicad_symbol_lib (version 20211014) (generator kicad_symbol_editor)
  (symbol "DAC5571" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -8.89 13.97 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "DAC5571" (id 1) (at 1.27 13.97 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Package_TO_SOT_SMD:SOT-23-6" (id 2) (at 17.78 0 0)
      (effects (font (size 1.27 1.27) italic) hide)
    )
    (property "Datasheet" "https://www.ti.com/lit/ds/symlink/dac5571.pdf" (id 3) (at 0 -5.08 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "dac" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "8-BIT, LOW-POWER, VOLTAGE OUTPUT, I2C INTERFACE DIGITAL-TO-ANALOG CONVERTER" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_fp_filters" "MSOP-10*" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "DAC5571_0_1"
      (rectangle (start -8.89 1.27) (end 8.89 12.7)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
    (symbol "DAC5571_1_1"
      (pin output line (at 12.7 10.16 180) (length 3.81)
        (name "VOUT" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 -2.54 90) (length 3.81)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 16.51 270) (length 3.81)
        (name "AVDD" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 10.16 0) (length 3.81)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 7.62 0) (length 3.81)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 3.81 0) (length 3.81)
        (name "A0" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "DAC5574" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -8.89 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "DAC5574" (id 1) (at 3.81 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Housings_SSOP:MSOP-10_3x3mm_Pitch0.5mm" (id 2) (at 22.86 -2.54 0)
      (effects (font (size 1.27 1.27) italic) hide)
    )
    (property "Datasheet" "http://www.ti.com/lit/ds/symlink/dac5574.pdf" (id 3) (at 0 -5.08 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "dac" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "QUAD, 8-BIT, LOW-POWER, VOLTAGE OUTPUT, I2C INTERFACE DIGITAL-TO-ANALOG CONVERTER" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_fp_filters" "MSOP-10*" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "DAC5574_0_1"
      (rectangle (start -8.89 -1.27) (end 8.89 13.97)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
    (symbol "DAC5574_1_1"
      (pin bidirectional line (at 12.7 10.16 180) (length 3.81)
        (name "VOUTA" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 1.27 0) (length 3.81)
        (name "A1" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 7.62 180) (length 3.81)
        (name "VOUTB" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 -5.08 90) (length 3.81)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 5.08 180) (length 3.81)
        (name "VOUTC" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 2.54 180) (length 3.81)
        (name "VOUTD" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 7.62 0) (length 3.81)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 10.16 0) (length 3.81)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 17.78 270) (length 3.81)
        (name "AVDD" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 3.81 0) (length 3.81)
        (name "A0" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "DAC5578" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -8.89 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "DAC5578" (id 1) (at 3.81 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Housings_SSOP:TSSOP-16_4.4x5mm_Pitch0.65mm" (id 2) (at 25.4 -12.7 0)
      (effects (font (size 1.27 1.27) italic) hide)
    )
    (property "Datasheet" "" (id 3) (at 0 -5.08 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_fp_filters" "TSSOP-16*" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "DAC5578_0_1"
      (rectangle (start -8.89 -11.43) (end 8.89 13.97)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
    (symbol "DAC5578_1_1"
      (pin input line (at -12.7 -1.27 0) (length 3.81)
        (name "~{LDAC}" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -7.62 180) (length 3.81)
        (name "VOUTH" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -2.54 180) (length 3.81)
        (name "VOUTF" (effects (font (size 1.27 1.27))))
        (number "11" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 2.54 180) (length 3.81)
        (name "VOUTD" (effects (font (size 1.27 1.27))))
        (number "12" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 7.62 180) (length 3.81)
        (name "VOUTB" (effects (font (size 1.27 1.27))))
        (number "13" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 -15.24 90) (length 3.81)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "14" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 10.16 0) (length 3.81)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "15" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 7.62 0) (length 3.81)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "16" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 -7.62 0) (length 3.81)
        (name "ADDR0" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -1.27 17.78 270) (length 3.81)
        (name "AVDD" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 10.16 180) (length 3.81)
        (name "VOUTA" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 5.08 180) (length 3.81)
        (name "VOUTC" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 0 180) (length 3.81)
        (name "VOUTE" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 -5.08 180) (length 3.81)
        (name "VOUTG" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin passive line (at -12.7 2.54 0) (length 3.81)
        (name "VREFIN" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -12.7 -3.81 0) (length 3.81)
        (name "~{CLR}" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "DAC6574" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -8.89 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "DAC6574" (id 1) (at 3.81 15.24 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Housings_SSOP:MSOP-10_3x3mm_Pitch0.5mm" (id 2) (at 22.86 -2.54 0)
      (effects (font (size 1.27 1.27) italic) hide)
    )
    (property "Datasheet" "https://www.ti.com/lit/ds/symlink/dac6574.pdf" (id 3) (at 0 -5.08 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "IC DAC 10BIT V-OUT" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_fp_filters" "MSOP-10*" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "DAC6574_0_1"
      (rectangle (start -8.89 -1.27) (end 8.89 13.97)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
    (symbol "DAC6574_1_1"
      (pin bidirectional line (at 12.7 10.16 180) (length 3.81)
        (name "VOUTA" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 1.27 0) (length 3.81)
        (name "A1" (effects (font (size 1.27 1.27))))
        (number "10" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 7.62 180) (length 3.81)
        (name "VOUTB" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 -5.08 90) (length 3.81)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 5.08 180) (length 3.81)
        (name "VOUTC" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 12.7 2.54 180) (length 3.81)
        (name "VOUTD" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 7.62 0) (length 3.81)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -12.7 10.16 0) (length 3.81)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 17.78 270) (length 3.81)
        (name "AVDD" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at -12.7 3.81 0) (length 3.81)
        (name "A0" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
    )
  )
)
